import urllib
import os
from datetime import datetime
from http.cookiejar import CookieJar
import urllib.request


DEBRIS_URL = "https://www.space-track.org/#catalog"

class SatelliteRetriever:
    def __init__(self, config):
        self.last_login = datetime(year=1900, month=1, day=1)
        self.credentials = {
            "identity" : config["username"],
            "password" : config["password"]
        }
        self.DEBUG = config["debug_level"]
        self.CJ = CookieJar()
        self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.CJ))
        self.lists = {}
    
    def get_debris_numbers(self):
        numbers =[]
        #f = open('debris_yno.txt','r')
        f = open('debris_tianhui4.txt','r')
        rows = f.readlines()
        for row in rows:
            values = row.split()            
            numbers.append(int(values[0].strip()))                               
            self.lists["debris"] = {"numbers" : sorted(numbers)}        
        return sorted(numbers)    

    def log_in(self):
        """Log in to space-track.org and store the cookie (expired in ~2 hours).
        Returns True is the log-in succeeded and False if it failed."""
        
        if self.DEBUG >= 2:
            print("SatelliteRetriever: Logging in...")
            
        data = urllib.parse.urlencode(self.credentials).encode("ascii")
        try:
            response = self.opener.open("https://www.space-track.org/ajaxauth/login", data)
        except urllib.error.HTTPError as e:
            print("SatelliteRetriever: {}".format(str(e)))
            return False
        except urllib.error.URLError as e:
            print("SatelliteRetriever: {}".format(str(e)))
            return False
            
        result = response.read().decode("utf-8")
        if "Failed" in result:
            print("SatelliteRetriever: Login failed!")
            return False
        self.last_login = datetime.now()
        return True

    def download_data(self):
        """Go through all satellite lists and download the orbital elements."""
        
        if self.DEBUG >= 1:
            print("SatelliteRetriever: Downloading data...")
        
        delta = datetime.now() - self.last_login
        if delta.seconds > 5400:
            if self.DEBUG >= 2:
                print("SatelliteRetriever: Log-in expired, retrying...")
            ok = self.log_in()
            if not ok:
                print("SatelliteRetriever: Log-in failed.")
                return False
        for listname in self.lists:
            IDs = self.lists[listname]["numbers"]            
  
            filename = "{}.txt".format(listname)
            if os.path.exists(filename):
                os.remove(filename)  
            filename = "{}.txt".format(listname)
            query = "/".join([
                "https://www.space-track.org/basicspacedata/query",
                "class/tle_latest/ORDINAL/1",
                "NORAD_CAT_ID/{}".format(",".join([str(id) for id in IDs])),
                "format/3le"
            ])            
            
            if self.DEBUG >= 2:
                print("SatelliteRetriever: Requesting elements for '{}'...".format(listname))
            try:
                response = self.opener.open(query)
            except urllib.error.HTTPError as e:
                print("SatelliteRetriever: {}".format(str(e)))
                return False
            data = response.read().decode("utf-8")
            data = data.replace("\r", "")            
            with open(filename, "w") as f:
                if self.DEBUG >= 2:
                    print("SatelliteRetriever: Saving to {}...".format(filename))
                f.write(data)
        return True               

from ExampleConfig import spacetrack_config

if __name__=="__main__":
    S = SatelliteRetriever(spacetrack_config)
    S.log_in()
    S.get_debris_numbers()
    S.download_data()
    
