For calculating the CPF file from TLE which can be downloaded from web space-track automatically, then transformation from CPF to elevation and azimuth. 

Contact: zhangcsh5@outlook.com

pip install -r requirements

Usage:

windows: 

run down_tle_gen_cpf.bat 

before the double-click down_tle_gen_cpf.bat, changing the compilation environment in down_tle_gen_cpf.bat

find the absolute path of tle2cpf source code, if you put in d:/, then you can open the path,

cd d:/tle2cpf

(else if you put the fold in e:/, 'cd e:/tle2cpf' instead)

then find the absolute path of python.exe, e.g. d:/anaconda3/python.exe, or maybe in e:/anaconda3/python.exe

d:/anaconda3/python.exe get_debrics_TLE_input_id.py 

d:/anaconda3/python.exe debris_cpf.py

d:/anaconda3/python.exe cal_ele_azi.py 

linux:

'bash down_tle_gen_cpf.sh' run in terminal


