# author: zhangcsh
# contact zhangcsh5@mail2.sysu.edu.cn

from skyfield.api import load, EarthSatellite
from datetime import datetime as dt
from datetime import timedelta as td
from astropy.time import Time
from os import path,makedirs
from skyfield.constants import AU_KM
import numpy as np
import os
fname = 'debris.txt'
f = open(fname,'r')
lines = f.readlines()   
rec_times = int(len(lines)/3)
for i in range(rec_times):

    line0 = lines[3*i]
    #print(line0)
    line1 = lines[3*i+1]
    line2 = lines[3*i+2]

    sat = EarthSatellite(line1,line2)

    line2_split = line2.split()
    s0 = line0.split()
    s2 = line2.split()
    
    if len(s0)==2:
        satellite = '-'.join((s0[1]))
    else:
    
        satellite = '-'.join((s0[1],s0[2]))    

    ts = load.timescale()
    step = 120 # s
    day = 10
    date = dt.utcnow()
    date_6 = date + td(days = day)
    nnn = str(int(date.strftime("%j")) + 500)
    v = '1'
    nnnv = ''.join([nnn,v])
    sate = satellite.lower()
    ID = s2[1]
    # Day = np.arange(date.day, date.day+5, 1)


    cpf_fold = 'online/'
    if os.path.exists(cpf_fold):
        dir_cpf_to = cpf_fold+Time(date).strftime('%Y%m%d') + '/'
    if not path.exists(dir_cpf_to): 
        makedirs(dir_cpf_to)   

    output_f = open(dir_cpf_to+
                    ID+"_cpf"+"_"+date.strftime("%y")+date.strftime("%m")+
                    date.strftime("%d")+"_"+ ''.join([nnn,'1'])+".tle",'w')
    seconds = np.arange(0, 86400 * day, step)
    ''' LINE 1
    Header type 1 Basic information (required) 
    1-2 A2 Record Type (= "H1")
    4-6 A3 "CPF"
    8-9 I2 Format Version
    12-14 A3 Ephemeris Source (e.g., "HTS ", "UTX ")
    16-19 I4 Year of ephemeris production
    21-22 I2 Month of ephemeris production
    24-25 I2 Day of ephermeris production
    27-28 I2 Hour of ephemeris production (UTC)
    31-34 I4 Ephemeris Sequence number
    36-45 A10 Target Name
    47-56 A10 Notes (e.g., "041202","DE-403")
    example: H1 CPF  1  SGF 2020  5 25  2  6461 ajisai   '''
    "%2d  %3c %4d %2d %2d %2d  %4d %10c %10s"
    output_f.write("%2s %3s %2s %4s %4s %2s %2s %2s %5s %8s\n" % \
                ('H1','CPF','1', 'SYS', date.strftime("%Y"),
                    date.strftime("%m"), date.strftime("%d"),
                    date.strftime("%H"), nnnv, ID))
    '''
    LINE 2
    Header type 2 Basic information - 2 (required)
    1-2  A2  Record Type (= "H2")
    4-1  1I8 COSPAR ID
    13-1  6I4 SIC
    18-2  5I8 NORAD ID
    27-30 I4 Starting Year
    32-33 I2 Starting Month
    35-36 I2 Staring Day
    38-39 I2 Starting Hour (UTC)
    41-42 I2 Starting Minute (UTC)
    44-45 I2 Starting Second (UTC)
    47-50 I4 Ending Year
    52-53 I2 Ending Month
    55-56 I2 Ending Day
    58-59 I2 Ending Hour (UTC)
    61-62 I2 Ending Minute (UTC)
    64-65 I2 Ending Second (UTC)
    67-71 I5 Time between table entries (UTC seconds)(=0 if variable)
    73 I1 Compatibility with TIVs = 1 (=> integratable, geocentric ephemeris)
    75 I1 Target type
    1 = passive artificial satellite
    2 = passive lunar reflector
    3 = synchronous transponder
    4 = asynchronous transponder
    77-78 I2 Reference frame
    0 = geocentric true body fixed (default)
    1 = geocentric space fixed (i.e. Inertial) (True of Date)
    2 = geocentric space fixed (Mean of Date J2000)
    80 I1 Rotational angle type
    0 = Not Applicable
    1 = Lunar Euler angles: ?, ?, and ?
    2 = North pole Right Ascension and Declination, and angle to prime meridian (?0, ?0, and W)
    82 I1 Centre of mass correction
    0 = none applied, predcition is for centre of mass of target
    1 = applied, prediction is for retro-reflector array
    example: H2  8606101 1500    16908 2020  5 24  0  0  0 2020  5 29 23 55  0   240 1 1  0 0 0
    '''
    
    output_f.write("%s %8s %4s %8s %4s %2s %2s %2s %2s %2s %4s %2s %2s %2s %2s %2s %5d %1d %1d %2d %1d %1d\n" % \
                ('H2', 1909301, 2033, sat.model.satnum, date.strftime("%Y"),
                    date.strftime("%m"), date.strftime("%d"),0,0,0,
                    date_6.strftime("%Y"),date_6.strftime("%m"),
                    date_6.strftime("%d"),0,0,0,step, 1, 1, 0, 0, 0))
    output_f.write("%s\n" % \
                'H9')
    for second in seconds:
        t = ts.utc(year = date.year, month = date.month, day = date.day,
            hour = 0, minute = 0, second = second)
        
        #jd = JulianDate((date.year, date.month, date.day, 0, 0, second))
        jd = ts.tai(year = date.year, month = date.month, day = date.day,
            hour = 0, minute = 0, second = second)


        rv0 = sat.at(t)
        #sunlit = sat.at(t).is_sunlit(eph)
        #print(sunlit)
        rv0_ITRF = sat.ITRF_position_velocity_error(t)
        r0_ITRF = rv0_ITRF[0] * AU_KM * 1e3 #m
        output_f.write("%2s %s %.0f    %.5f %s  %.3f   %.3f   %.3f\n" % \
                        ('10','0', np.floor(jd.tai - 2400000.5), second - np.floor(second/86400)*86400,
                        0, r0_ITRF[0], r0_ITRF[1], r0_ITRF[2]))
    '''
    Record type 99 Ephemeris Trailer (last record in ephemeris)
    1-2 A2 Record Type (= "99")
    '''
    output_f.write("%s\n" % \
                ('99'))
    output_f.close() 
    print(ID+"_cpf"+"_"+date.strftime("%y")+date.strftime("%m")+
                    date.strftime("%d")+"_"+ ''.join([nnn,'1']))
