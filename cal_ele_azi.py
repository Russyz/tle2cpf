import numpy as np
import math
import erfa
import sys
from ctypes import *
import os
from datetime import datetime as dt
from scipy import interpolate
abs_dir = os.getcwd()

def cal_ele(dxyzt):
    '''
    Parameters
    ----------
    dxyzt : float
        reflector coordinates in ITRS, given in [meter]
    
    '''
    dxt = dxyzt[0]
    dyt = dxyzt[1]
    dzt = dxyzt[2]
    
    LAMBDA,PHI,_ = erfa.gc2gd(1,xsta)
    sp3R   = np.linalg.norm([dxt,dyt,dzt])
    gvs = np.zeros(3)
    gvs[0] =  math.cos(PHI)* math.cos(LAMBDA)			# Station X unit vector
    gvs[1] =  math.cos(PHI)* math.sin(LAMBDA)			# Station Y unit vector
    gvs[2] =  math.sin(PHI)					# Station Z unit vector
    dstn   =  np.linalg.norm(gvs)	# Normalise the unit vectors
    czd    =  (dxt*gvs[0]+dyt*gvs[1]+dzt*gvs[2])/(sp3R*dstn)		# Zenith height component of SAT->STAT vector / vector range
    altc   =  math.asin(czd)*360.0/(2.0*np.pi)
    return altc

# calculate the azimuth of retro-reflectors
def cal_azi(dxyzt):
    dxt = dxyzt[0]
    dyt = dxyzt[1]
    dzt = dxyzt[2]

    LAMBDA,PHI,H = erfa.gc2gd(1,xsta)
    gvx = np.zeros(3)
    gvx[0] = -math.sin(PHI)*math.cos(LAMBDA)
    gvx[1] = -math.sin(PHI)*math.sin(LAMBDA)
    gvx[2] = math.cos(PHI)

    gvy =  np.zeros(3)
    gvy[0] = -math.sin(LAMBDA)
    gvy[1] = math.cos(LAMBDA)
    gvy[2] = 0

    cx = (dxt*gvx[0]+dyt*gvx[1]+dzt*gvx[2])
    cy = (dxt*gvy[0]+dyt*gvy[1]+dzt*gvy[2])
    azic = math.atan2(cy, cx) * erfa.DR2D
    if azic < 0:
        azic += 360
    return azic

def sec2hms(utsec): 
    # ------------------------  implementation   ------------------
    temp = utsec / 3600.0
    hr = np.floor(temp)
    min = np.floor((temp - hr) * 60.0)
    sec = (temp - hr - min / 60.0) * 3600.0
    return hr,min,sec

if __name__ == '__main__':
    ymd = dt.utcnow().strftime('%Y%m%d')
    #print(ymd)
    fold_cpf = abs_dir + '/online/{}/'.format(ymd)
    files = os.listdir(fold_cpf)  
    xsta = [-2358691.210,5410611.484,2410087.607]     
    for file in files:
        if file.endswith('tle'):
            output_f = open(fold_cpf+'ele_azi.txt','w')
            output_f.write("#year month date hour minute second azimuth elevation\n")            
            time, x, y, z = [], [], [], []
            lines = open(fold_cpf + file,'r')
            for line in lines:
                s = line.split()
                if s[0] == '10':
                    time.append(np.double(s[2]) + np.double(s[3])/erfa.DAYSEC)
                    x.append(np.double(s[5]))
                    y.append(np.double(s[6]))
                    z.append(np.double(s[7]))
            kd = 'cubic'
            cpf_IntpX = interpolate.interp1d(time, x, kind=kd,fill_value="extrapolate")
            cpf_IntpY = interpolate.interp1d(time, y, kind=kd,fill_value="extrapolate")
            cpf_IntpZ = interpolate.interp1d(time, z, kind=kd,fill_value="extrapolate")
            min_time = np.min(time)
            max_time = np.max(time)
            
            for t in np.arange(min_time,max_time,1/erfa.DAYSEC):                
                intep_xyz = np.array([cpf_IntpX(t),cpf_IntpY(t),cpf_IntpZ(t)]) - xsta
                #intep_xyz = lagint(time,[x,y,z],t,9) - xsta
                
                ele = cal_ele(intep_xyz)                                
                jd1 = 2400000.5
                Y, M, D, fd = erfa.jd2cal(jd1, t)                
                h,m,s = sec2hms(fd * erfa.DAYSEC)
                if ((ele > 15) and (ele < 90)) :
                    azi = cal_azi(intep_xyz)
                    output_f.write("%.0f %.0f %.0f %.0f %.0f %.3f %.4f %.4f\n" % \
                        (Y, M, D, h, m, s, azi, ele))                
            output_f.close()
        #print(intep_xyz)
        #np.loadtxt(fold_cpf + file,skiprows=3 -1)
